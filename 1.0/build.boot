(set-env!
  :source-paths #{"src/cljs" "src/cljc"}
  :resource-paths #{"src/html"}

  :dependencies '[;; app deps
                  [cljsjs/create-react-class "15.6.3-1"]
                  [cljsjs/immutable "3.8.1-0"]
                  [cljsjs/react "16.6.0-0"]
                  [cljsjs/react-dom "16.6.0-0"]
                  [cljsjs/react-dom-server "16.6.0-0"]
                  [cljsjs/slate "0.33.6-0"]
                  [cljsjs/slate-react "0.12.6-0"]
                  [fulcrologic/fulcro "2.6.18" :exclusions [[cljsjs/react-dom] [cljsjs/react] [cljsjs/react-dom-server]]]
                  [org.clojure/clojure "1.10.0"]
                  [org.clojure/clojurescript "1.10.439"]
                  [org.clojure/spec.alpha "0.2.176"]

                  ;; dev deps
                  [devcards "0.2.6" :exclusions [[cljsjs/react-dom] [cljsjs/react]]]
                  [fulcrologic/fulcro-inspect "2.2.4"]
                  [org.clojure/test.check "0.10.0-alpha3"]

                  ;; boot tasks
                  [adzerk/boot-cljs "2.1.5" :scope "test"]
                  [adzerk/boot-cljs-repl "0.4.0" :scope "test"]
                  [adzerk/boot-reload "0.6.0" :scope "test"]
                  [binaryage/devtools "0.9.10" :scope "test"]
                  [binaryage/dirac "RELEASE" :scope "test"]
                  [cider/piggieback "0.3.10" :scope "test"]
                  [nrepl "0.5.3" :scope "test"]
                  [pandeiro/boot-http "0.8.3" :scope "test"]
                  [powerlaces/boot-cljs-devtools "0.2.0" :scope "test"]
                  [weasel "0.7.0" :scope "test"]])

(require '[adzerk.boot-cljs :refer [cljs]]
         '[adzerk.boot-cljs-repl :refer [cljs-repl start-repl]]
         '[adzerk.boot-reload :refer [reload]]
         '[pandeiro.boot-http :refer [serve]]
         '[powerlaces.boot-cljs-devtools :refer [cljs-devtools dirac]])

(deftask build-cljs
  "Build ClojureScript files into JavaScript"
  []
  (comp
    (cljs :source-map true)
    (target :dir #{"target"})))

(deftask dev
  "Launch development environment"
  []
  (comp
    (serve :dir "target")
    (watch)
    (reload)
    #_(cljs-repl)
    (cljs-devtools)
    #_(dirac)
    (build-cljs)))
