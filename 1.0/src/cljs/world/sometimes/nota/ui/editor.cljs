(ns world.sometimes.nota.ui.editor
  (:require
   [cljsjs.slate]
   [cljsjs.slate-react]
   [clojure.string :as string]
   [create-react-class]
   [fulcro.client.dom :as dom]
   [fulcro.client.primitives :as prim]
   [goog.object :as g.object]
   [react])
  (:import
   (goog.date UtcDateTime)
   (goog.i18n DateTimeFormat)))

(prim/defsc FormMetadata [component {:world.sometimes.nota.meta/keys
                                     [name created tags updated]
                                     :as props}]
  {:ident
   (fn [] [:component `FormMetadata])
   :query
   [:world.sometimes.nota.meta/name
    :world.sometimes.nota.meta/created
    :world.sometimes.nota.meta/tags
    :world.sometimes.nota.meta/updated]
   :initial-state
   (fn [_]
     (let [now (UtcDateTime.)]
       {:world.sometimes.nota.meta/name "test"
        :world.sometimes.nota.meta/created now
        :world.sometimes.nota.meta/tags #{:tag1 :tag2}
        :world.sometimes.nota.meta/updated now}))}
  (let [date-time-format (DateTimeFormat. (.. DateTimeFormat -Format -SHORT_DATETIME))]
    (dom/fieldset
     {:aria-labelledby "metadata-heading"
      :role "region"}
     (dom/h2
      {:id "metadata-heading"
       :role "heading"}
      "Metadata")
     (dom/div
      (dom/label
       {:htmlFor "name"}
       "Name")
      (dom/input
       {:id "name"
        :name "name"
        :value name}))
     (dom/div
      (dom/label
       {:htmlFor "tags"}
       "Tags")
      (dom/input
       {:id "tags"
        :name "tags"
        :value (string/join " " tags)}))
     (dom/div
      (dom/label
       {:htmlFor "created"}
       "Created")
      (dom/input
       {:id "created"
        :name "created"
        :value (.format date-time-format created)
        :readOnly true}))
     (dom/div
      (dom/label
       {:htmlFor "updated"}
       "Updated")
      (dom/input
       {:id "updated"
        :name "updated"
        :value (.format date-time-format updated)
        :readOnly true})))))

(def ui-form-metadata (prim/factory FormMetadata))

(defn field-id [id]
  (str "field-" id))

(defn field-type [type]
  (subs (str type) 1))

(defmulti render-field :world.sometimes.nota.field/type)

(defmethod render-field :text/plain [{:keys [db/id]
                                      :world.sometimes.nota.field/keys
                                      [type data]}]
  (dom/textarea
   {:id (field-id id)
    :name (field-id id)
    :value (first data)}))

(defmethod render-field :text/rich [{:keys [db/id]
                                     :world.sometimes.nota.field/keys
                                     [type data]}]
  (dom/textarea
   {:id (field-id id)
    :name (field-id id)
    :value (first data)}))

(defmethod render-field :link/general [{:keys [db/id]
                                        :world.sometimes.nota.field/keys
                                        [type data]}]
  (let [the-field-id (field-id id)
        link-id (str "field-link-" id)
        [url] data]
    (dom/create-element
     react/Fragment
     nil
     (dom/input
      {:aria-controls link-id
       :id the-field-id
       :name the-field-id
       :value url})
     (dom/a
      {:id link-id
       :href url
       :style {:display "block"}}
      "link"))))

(defmethod render-field :link/image [{:keys [db/id]
                                     :world.sometimes.nota.field/keys
                                     [type data]}]
  (let [the-field-id (field-id id)
        image-id (str "field-image-" id)
        [url] data]
    (dom/create-element
     react/Fragment
     nil
     (dom/input
      {:aria-controls image-id
       :id the-field-id
       :name the-field-id
       :value url})
     (dom/a
      {:href url
       :style {:display "block"}}
      (dom/img
       {:id image-id
        :src url})))))

(defmethod render-field :default [{:world.sometimes.nota.field/keys
                                   [type]}]
  (dom/p (str type)))

(prim/defsc Field [component {:keys
                              [db/id]
                              :world.sometimes.nota.field/keys
                              [type]
                              :as props}]
  {:ident
   [:world.sometimes.nota.field/by-id :db/id]
   :query
   [:db/id
    :world.sometimes.nota.field/type
    :world.sometimes.nota.field/data]
   :initial-state
   (fn [{:keys [id type data]}]
     {:db/id id
      :world.sometimes.nota.field/type type
      :world.sometimes.nota.field/data data})}
  (dom/div
   (dom/label
    {:htmlFor (field-id id)
     :style {:display "block"}}
    (field-type type))
   (render-field props)))

(def ui-field (prim/factory Field {:keyfn :db/id}))

(prim/defsc FormFields [component fields]
  {:ident
   (fn [] [:component `FormFields])}
  (apply
   dom/fieldset
   {:aria-labelledby "fields-heading"
    :role "region"}
   (dom/h2
    {:id "fields-heading"
     :role "heading"}
    "Fields")
   (map ui-field fields)))

(def ui-form-fields (prim/factory FormFields))

(def Document (g.object/get js/Slate "Document"))
(def Value (g.object/get js/Slate "Value"))
(def Block (g.object/get js/Slate "Block"))
(def Text (g.object/get js/Slate "Text"))
(def SlateEditor (g.object/get js/SlateReact "Editor"))

(def EditorImpl
  (create-react-class
   #js {:displayName "EditorImpl"
        :getInitialState
        (fn []
          (this-as component
            #js {:value (g.object/getValueByKeys
                         component "props" "value")}))
        :render
        (fn []
          (this-as component
            (let [value (g.object/getValueByKeys
                         component "state" "value")
                  onChange (g.object/getValueByKeys
                            component "props" "onChange")]
              (dom/create-element
               SlateEditor
               #js {:value value
                    :onChange (fn [& args]
                                (apply onChange component args))}))))}))

(def ui-editor-impl (react/createFactory EditorImpl))

(prim/defsc EditorForm [component {:world.sometimes.nota.note/keys
                                   [fields meta]
                                   :as props}]
  {:ident
   (fn [] [:component `EditorForm])
   :query
   [:world.sometimes.nota.note/fields
    {:world.sometimes.nota.note/meta
     (prim/get-query FormMetadata)}]
   :initial-state
   (fn [_]
     {:db/id
      (random-uuid)
      :world.sometimes.nota.note/fields
      [(prim/get-initial-state
        Field
        {:id (random-uuid)
         :type :text/plain
         :data ["Plain text field"]})]
      :world.sometimes.nota.note/meta
      (prim/get-initial-state FormMetadata nil)})}
  (let [value (.fromJS
               Value
               (clj->js
                {:document
                 {:nodes
                  [{:object :block
                    :type :text/plain
                    :nodes
                    [{:object :text
                      :leaves
                      [{:text "some plain text"}]}]}]}}))]
    (dom/form
     :.c-editor__form
     (ui-editor-impl
      #js {:value value
           :onChange
           (fn [editor event]
             (.setState
              editor
              #js {:value
                   (g.object/get event "value")}))})
     #_(form-metadata/ui-form-metadata meta)
     #_(ui-form-fields fields))))

(def ui-editor-form (prim/factory EditorForm))

(prim/defsc Editor [component {:world.sometimes.nota.editor/keys
                               [active-note working-notes]
                               :as props}]
  {:ident
   (fn [] [:component `Editor])
   :query
   [{:world.sometimes.nota.editor/active-note
     (prim/get-query EditorForm)}
    :world.sometimes.nota.editor/working-notes]
   :initial-state
   (fn [_]
     (let [note (prim/get-initial-state EditorForm nil)]
       {:world.sometimes.nota.editor/active-note
        note
        :world.sometimes.nota.editor/working-notes
        [note]}))}
  (dom/div :.c-editor
   (ui-editor-form active-note)))

(def ui-editor (prim/factory Editor))
