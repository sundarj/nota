(ns world.sometimes.nota.ui.devcards.app
  (:require
   [fulcro.client.cards :refer [defcard-fulcro]]
   [world.sometimes.nota.ui :as ui]))

(let [note {:world.sometimes.nota.note/id
            (random-uuid)
            :world.sometimes.nota.note/fields
            []
            :world.sometimes.nota.note/meta
            {:world.sometimes.nota.meta/name "test"
             :world.sometimes.nota.meta/position nil
             :world.sometimes.nota.meta/tags #{}}}]
  (defcard-fulcro Root
    "What the user will first see upon app start"
    ui/Root
    note
    {:inspect-data true}))
