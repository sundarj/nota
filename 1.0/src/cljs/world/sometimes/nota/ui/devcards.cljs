(ns world.sometimes.nota.ui.devcards
  (:require
   [devcards.core :as devcards]
   [world.sometimes.nota.ui.devcards.app]
   [world.sometimes.nota.ui.devcards.editor]
   [world.sometimes.nota.ui.devcards.fields]))


(defn main []
  (devcards/start-devcard-ui!))
