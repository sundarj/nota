(ns world.sometimes.nota.ui
  (:require
   [fulcro.client]
   [fulcro.client.dom :as dom]
   [fulcro.client.primitives :as prim]
   [goog]
   [world.sometimes.nota.ui.editor :as editor]))

(enable-console-print!)

(prim/defsc Root [component {:world.sometimes.nota.app/keys
                             [editor]
                             :as props}]
  {:query
   [{:world.sometimes.nota.app/editor
     (prim/get-query editor/Editor)}]
   :initial-state
   (fn [_]
     {:world.sometimes.nota.app/editor
      (prim/get-initial-state editor/Editor nil)})}
  (dom/main
   (editor/ui-editor editor)))

(defonce app
  (atom (fulcro.client/new-fulcro-client)))

(defn start []
  (swap! app fulcro.client/mount Root "app"))
