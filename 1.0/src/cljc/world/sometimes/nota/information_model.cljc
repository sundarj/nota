(ns world.sometimes.nota.information-model
  (:require
   [clojure.spec.alpha :as spec]
   [clojure.string :as string]))

(spec/def :db/id uuid?)

(spec/def :db/timestamp
  (fn [x]
    #?(:clj (instance? java.time.Instant x)
       :cljs (instance? goog.date.UtcDateTime x))))

(spec/def :world.sometimes.nota.field/type qualified-keyword?)

(spec/def :world.sometimes.nota.field/data
  (spec/coll-of any?
                :kind vector?))

(spec/def :world.sometimes.nota.note/field
  (spec/keys :req [:db/id
                   :world.sometimes.nota.field/type
                   :world.sometimes.nota.field/data]))

(spec/def :world.sometimes.nota.meta/name
  (spec/and string?
            (complement string/blank?)))

(spec/def :world.sometimes.nota.meta/description string?)

(spec/def :world.sometimes.nota.meta/tag keyword?)

(spec/def :world.sometimes.nota.meta/tags
  (spec/coll-of :world.sometimes.nota.meta/tag
                :kind set?))

(spec/def :world.sometimes.nota.meta/position pos-int?)

(spec/def :world.sometimes.nota.meta/created :db/timestamp)
(spec/def :world.sometimes.nota.meta/updated :db/timestamp)

(comment
  (spec/valid? :world.sometimes.nota.meta/created (java.time.Instant/now))
  )

(spec/def :world.sometimes.nota.note/meta
  (spec/keys :req [:world.sometimes.nota.meta/name
                   :world.sometimes.nota.meta/tags
                   :world.sometimes.nota.meta/created
                   :world.sometimes.nota.meta/updated]
             :opt [:world.sometimes.nota.meta/description
                   :world.sometimes.nota.meta/position]))

(spec/def :world.sometimes.nota.note/fields
  (spec/coll-of :world.sometimes.nota.note/field
                :kind vector?
                :distinct true))

(spec/def :world.sometimes.nota.app/note
  (spec/keys :req [:db/id
                   :world.sometimes.nota.note/fields
                   :world.sometimes.nota.note/meta]))

(spec/def :world.sometimes.nota.editor/working-notes
  (spec/coll-of :world.sometimes.nota.app/note
                :kind vector?
                :distinct true))

(spec/def :world.sometimes.nota.editor/active-note
  :world.sometimes.nota.app/note)

;; some
(defn active-note-is-working-note?
  [{:world.sometimes.nota.editor/keys [working-notes active-note]}]
  (seq
   (filter (partial = active-note)
           working-notes)))

(spec/def :world.sometimes.nota.app/editor
  (spec/and (spec/keys :req [:world.sometimes.nota.editor/working-notes
                             :world.sometimes.nota.editor/active-note])
            active-note-is-working-note?))

(spec/def :world.sometimes.nota.app/notes
  (spec/coll-of :world.sometimes.nota.app/note
                :kind vector?))

(spec/def :world.sometimes.nota/app
  (spec/keys :req [:world.sometimes.nota.app/editor
                   :world.sometimes.nota.app/notes]))

