* Summary
Nota is a note-taking app for persisting thoughts, reminders, todos, and urls.
* Rationale
Using the file system to store notes gets unwieldy fast. Text files are also not
as rich, interative, and readable as web pages; and cannot be viewed or worked
on remotely.
* Objectives
** Rich
Plain text is not good enough. There should be a nice story for mixing text,
rich text, images, urls, and datetimes at least.
** Readable
It should be a pleasure to read, with excellent typography, and powerful
hierachy, metadata, search, and filtering/folding features.
** Open
Note - from Latin nota ‘a mark’, notare ‘to mark’. It should allow for free and
fast jotting down of notes, as well as more organised writing. It should impose
no limits on note-taking; should work like physical note-taking with paper and
pencil. It should be able to start from a flat list of notes to grouped, and move
towards an organised, hierachical note system.
** Interactive
The functionality should derive from user agency, not against it. The user
should be able to get into a flow, whether by keyboard or mouse, and exploit the
UI for their purposes; the UI should be tangible and mouldable.
** Immutable
No thoughts should ever be lost. If the user wants to remove a thought, it shoul
d be archived; still accessible, rather than removed entirely.
** Offline
It should work offline as well as online, just like text files.
* Prior Art
https://todoist.com
https://habitica.com
https://orgmode.org
https://www.datomic.com
https://git-scm.com
https://gitlab.com/Numergent/memento
https://vivaldi.com
