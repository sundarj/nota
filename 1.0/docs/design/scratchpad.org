* usecases
collection of frequently-used urls for easy access (i.e. pinned tabs, start page)
bookmarks (can be added via browser extension)
* misc
- https://twitter.com/tannerc/status/994614177682477056
seems like the problem there is the transience of tooltips
would be solved by having a global log of tooltips / actions
that the user can easily review / replay (edited)
making tooltips immutable :~)
- https://www.slatejs.org
- https://open-platform.theguardian.com
persist guardian articles linked to locally? do so for other notes?
* rules
- https://github.com/tonsky/datascript
- https://github.com/replikativ/datahike
- https://github.com/cerner/clara-rules
- https://github.com/CoNarrative/precept
- http://vvvvalvalval.github.io/posts/datascript-as-a-lingua-franca-for-domain-modeling.html

tony.kay [19 minutes ago]
The short answer that comes to mind, @sundarj, is that you could replace the
client database and parser…the parser query-side would query for “facts” in
Clara, and the mutations would insert/retract facts.  Not sure how it would
perform, but it would give you a clear decoupling…that said, you’d be back to
Om Next since you’d then have to figure out how to do the data-driven networking
story and graph queries.

tony.kay [17 minutes ago]
My current use of Clara is for more focused problem solving…e.g. shopping cart
business rules.  Insert some items in the cart, run rules, then run queries
about things like shipping charges and discounts. Then persist those “results”
to the UI database.  It isn’t “automatic UI update” in that sense, but it gives
me a more functional interface to using rules to replace hand-coded logic. I
see Clara as a big function I can call to do complex logic and return a result
(rather than side-effecting in the rules themselves, which irks me)

tony.kay [16 minutes ago]
You’ve got this beautiful rules engine that is completely immutable, and yet
everyone still wants to side-effect external changes.  It’s convenient, but
truth-management is hard to tell how to “undo” side effects.

tony.kay [14 minutes ago]
So, what these UI frameworks are doing is tying the UI to facts in the database
by you defining queries that feed the UI…so inserts/retracts from the rules
update the UI

tony.kay [13 minutes ago]
that’s pretty cool…I’m still thinking about how that functions with the “rest of
the story” that you need…like server integration. (edited)

tony.kay [12 minutes ago]
I like the idea…hooking facts directly to UI.

sundarj [10 minutes ago]
i really like Fulcro's client-side and networking story, but i feel like the
server-side is more open-ended (with good reason). that clarifies things re: how
you use Clara. interesting! i think facts and rules engines are a really cool
idea, so am always curious about how they're used.

tony.kay [10 minutes ago]
There’s almost certainly an integration story here where Clara could maintain
facts, Pathom could run graph queries against the Clara queries

tony.kay [9 minutes ago]
thanks for asking….I’ll be munging this in my head for a bit :slightly_smiling_face:

sundarj [9 minutes ago]
haha, thanks for the in-depth response!

tony.kay [4 minutes ago]
might be interesting to make “derived” data be Clara…e.g. put in a parser that
uses the client database for hard data, and let certain keywords trigger queries
for clara facts. Then mutations would insert changes into the client db, and
add/retract relevant facts to clara…refresh of UI would then automatically
update derived data from clara…all sorts of possibilities.
* network outage
hmaurer [14:16]
Hi! I am looking for some advice on something. I am working on an application which involves an entity for which I want to track all changes as a sequence of events (aka that entity is event-sourced). When the user performs actions on the frontend I want them to be immedialy reflected on the state of the entity (aka added to the event log and reduced), but I also want them to be synced with the backend. My question is: how do I ensure that these events are synced in order?
If I simply send every event to the backend I believe there won’t be any order guarantee

tony.kay [15:43]
@hmaurer so, Fulcro guarantees that mutation order is preserved (and goes before reads).  This is per individual client.
Are you asking a distributed systems question?  i.e.  are you asking how to get a total order among all users?
Fulcro is quite good at normal event sourcing, though, because of the client guarantees, and the fact that the mutations are already "event-like"

hmaurer [15:45]
@tony.kay no. Perhaps I should give you a little bit of context. I am building an exercise platform for students. I want to store the sequence of actions that lead to a particular state in an exercise, as this is useful to later analyse and see where the students went wrong, etc
Exercise states aren’t shared; they’re per-student

tony.kay [15:45]
then you're fine. Fulcro will preserve order of mutations based on when the events occur

hmaurer [15:45]
Amazing. thank you :slightly_smiling_face: How does it do that?

tony.kay [15:46]
The network plumbing has a queue.  Each mutation is processed locally, then each is processed of remotes (in the order they appeared in the tx expression).  When remote-processed, they are placed on that outbound queue in that order
this preserves reasoning...it can be disabled, but it is on by default because otherwise things get hard to reason about

hmaurer [15:47]
And just to be clear: you are telling me that if I have the exercise state on the frontend, which I update on every action, and I have the exercise state on the backend, which I also update on every action (sent over the network), the frontend state and the backend state won’t go out of sync? (well, at least events will never be applied in the wrong order?)

tony.kay [15:47]
correct...though you will have to deal with error recovery yourself

hmaurer [15:47]
oh I see, so there is only ever one in-flight mutation

tony.kay [15:47]
e,g, network outage
yes
again, you can disable that, but it is the default so that reasoning is logical (edited)

hmaurer [15:48]
if there is a network outage the worst case scenario is that the backend state would be out of date, right?

tony.kay [15:48]
no, the worst case scenario is lost mutations
your UI has state that the back end does not

hmaurer [15:49]
So if I have mutations A, B, C (in that order) on the frontend, it’s possible (due to network outage) for the backend to see “A, C”?

tony.kay [20 minutes ago]
Yes, unless you add recovery logic for errors

hmaurer [20 minutes ago]
Ok. Thanks!

tony.kay [15:49]
If you use Fulcro's websocket support with auto-retry and make sure your mutations are idempotent, then that will auto-recover for you as well (and allow for easy server push back to the client) (edited)
idempotent is relatively easy: Give each mutation a UUID (generated when you call it, and sent as a parameter) so the server can know whether it has applied it or not...if it sees it twice, just ignore the repeat. (edited)

hmaurer [15:51]
Ah good point. I was going to say that it could be a bit annoying because some of my mutations are “add a line”
but your UUID solution sounds simple enough

tony.kay [15:51]
I think the websockets with auto-retry is the easiest, since the recovery is detected and managed at the network layer.
You could write a similar auto-recovery remote for normal xhr, but I have not done so...still, it's not that hard
* visuals
- https://secure2.entropay.com/consumer/u/registration
- https://scheme.cs61a.org
- https://www.maria.cloud
