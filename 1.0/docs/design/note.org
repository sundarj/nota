* Overview
The note is the most fundamental concept in the app. It is what the editor
edits; what the information sent between server and client will be about;
and what is serialized and persisted across sessions. Each note will be a bundle
of data; metadata pertaining to external concerns, as well as the actual content
of the note; title and other fields.
* Metadata
Assorted data about the note, like its tags.
* Field
Each field will represent a single semantic entity, such as a note's title; an
image; a URL; and so forth.
* Tags
Descriptive words pertaining to the note, that can be used to search for it.
They can be hierarchical, so that searching for a parent tag also shows notes
tagged with the tags below it in the hierarchy.
