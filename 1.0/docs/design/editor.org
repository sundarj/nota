* Overview
The editor will be the first and only thing the user sees upon app start; it is
the entry point for the rest of the app. It should allow you to edit any piece
of information pertaining to a particular note (metadata or otherwise),
via various types of input. It will also be the kernel around which the entire
rest of the app flows; in a similar vein to Emacs' buffers.
* Open notes
Can have open any number of notes at once, but will only be editing a single one
at a time.
* Inputs
Each input will correspond to a single field of the note being edited, and will
have behaviour specific to that field (an image upload for an image field, a
plain text input for the name, ...)
