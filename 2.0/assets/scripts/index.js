"use strict";

(function () {
  const selectOne = selector => document.querySelector(selector);
  const selectAll = selector => document.querySelectorAll(selector);

  const editButtons = selectAll(".c-resource__edit-btn");
  
  if (editButtons.length > 0) {
    for (let btn of editButtons) {
      btn.addEventListener("click", function (event) {
        const icon = btn.querySelector(".material-symbols-outlined");
        const state = icon.textContent === "edit" ? "pre-edit" : "editing";
        const tripRaw = btn.parentElement.parentElement.dataset.triple;
        const [sbj, prd, obj] = JSON.parse(tripRaw.replace("&quot;", '"'));
        
        if (state === "pre-edit") {
          icon.textContent = "check";

          const prdNode = btn.parentElement.querySelector(".js-predicate-text");
          const prdInput = document.createElement("input");
          prdInput.className = "w-5/6 bg-white border border-black p-1";
          prdInput.value = prd;
          prdNode.replaceWith(prdInput);

          const objNode = btn.parentElement.parentElement.querySelector(".c-resource__object-node");
          const objInput = document.createElement("textarea");
          objInput.className = "w-5/6 bg-white border border-black p-1 !mt-2";
          objInput.value = obj;
          objNode.replaceWith(objInput);
        } else {
          icon.textContent = "edit";

          const prdInput = btn.parentElement.querySelector("input");
          const prdInputValue = prdInput.value;
          const prdNode = document.createElement("span");
          prdNode.className = "js-predicate-text";
          prdNode.textContent = prdInput.value;
          prdInput.replaceWith(prdNode);

          const objInput = btn.parentElement.parentElement.querySelector("textarea");
          const objInputValue = objInput.value;
          const objNode = document.createElement("dd");
          objNode.className = "c-resource__object-node";
          objNode.innerHTML = marked.marked(objInput.value);
          objInput.replaceWith(objNode);

          fetch("/edit", {
            method: "PUT",
            headers: {
              "Content-Type": "application/json"
            },
            body: JSON.stringify({
              "erOldPredicate": prd,
              "erOldObject": obj,
              "erSubject": sbj,
              "erPredicate": prdInputValue,
              "erObject": objInputValue,
            })
          })
        }
      });
    }
  }
})();
