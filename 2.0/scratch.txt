appServer :: ST s (STRef s DB) -> Serv.Server AppRoutes 
appServer dbRef =
  indexHdr
  :<|> resourceHdr
  :<|> getNewHdr
  :<|> postNewHdr
  where
    indexHdr =
      let db = runST $ do            
                 ref <- dbRef
                 readSTRef ref
      in pure $ indexView db
    resourceHdr sbj =
      let db = runST $ do
                 ref <- dbRef
                 readSTRef ref
      in pure $ resourceView db sbj
    getNewHdr =
      let db = runST $ do
                 ref <- dbRef  
                 readSTRef ref
      in pure $ getNewView db
    postNewHdr formTriple = do
      let (MkFormTriple
            { ftSubject
            , ftPredicate
            , ftObject
            }) = formTriple
      let sbj = RDF.unode $ Text.pack ftSubject
      let prd = RDF.unode $ Text.pack ftPredicate
      let obj = RDF.LNode $ RDF.PlainL $
                  Text.pack ftObject
      let trip = RDF.triple sbj prd obj
      ref <- dbRef
      modifySTRef ref (flip RDF.addTriple trip)
      pure ()

-- serveDirectoryWebApp caches aggressively
fileServer :: Serv.Server FileRoutes
fileServer = Serv.serveDirectoryWith settings
  where settings = (defaultWebAppSettings "assets")
                     { WAST.ssMaxAge = WAST.NoCache }

app :: ST s (STRef s DB) -> Wai.Application
app dbRef = Serv.serve
              (Proxy :: Proxy Routes)
              (appServer dbRef :<|> fileServer)

main :: IO ()
main = do
  let p = RDF.TurtleParser
            (Just $ RDF.BaseUrl "https://veldi.dev")
            Nothing
  res <- RDF.parseFile p "assets/isopos.ttl"
  case res of
    Left (RDF.ParseFailure msg) ->
      putStrLn $ "error loading db: " <> msg
    Right db -> Warp.run 3000 (app $ newSTRef db)

