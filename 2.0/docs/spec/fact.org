* What is a fact?
A fact is 'a thing that is known or proved to be true' (OED); it's a true
statement about something. It has a subject (what it is about), a predicate
(what is happening), and an object (a word, phrase or description). For
example, 'Bob is a person' and 'the book has the title Reimagining Britain'. 
Some facts don't have an object however, e.g. 'Tony walks home' and 'Clojure
rocks'. Although, perhaps these facts wouldn't make useful notes.

Perhaps the user would want to make notes about something that isn't true,
like unicorns or atheism. But you can still make true statements about these
things. Unicorns have horns. Atheists believe that the universe generated
itself.

There are two possible options for facts:
- They can be created independently of notes that reference those facts. For
example, you first create a 'author: Justin Welby' fact then reference that
from a note about the book Reimagining Britain.
- They are just a name for note children, that can only be created within the
context of a note. A note is a collection of facts, but those facts don't have
any independent existence apart from notes.

I am leaning towards the first option. The second option has the downside that
you can't accumulate new facts about something, building on previous facts. You
would have to create an entirely new note and copy facts into it. The first
option also leaves it open for notes to be facts themselves that can then
recursively be referenced from other notes. Progressive fact-building is then
simply a matter of referencing old notes and adding new facts on top.