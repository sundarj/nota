* Name
Isopos - isos (same) + topos (place)
* Summary
A digital commonplace book. A home for life's significant information. Users
can store their everyday notes, arrange them by topic or for comparison, and
relate them to one another.

A user should be able to define a system of notes:
1. create notes
2. view notes
3. group notes
4. juxtapose notes, or groups of notes
5. link between notes
6. merge/combine notes
* Rationale
It is difficult keeping track of life's information, both transient and
persistent, both amusing and critical. I need a place to collect my notes and
resources together for reference.

Currently my note-taking process is a mix of physical paper notes, bookmarks,
google docs, and local and remote text files - oh, and a personal discord server
I use to send myself messages. I'm designing this Commonplace to try to
consolidate them all.

Ideally I'll be able to work on notes in org mode, and work on notes in the web
app, and they'll both end up in the same place somehow. Paper notes I'll scan in.
Bookmarks i guess I'll make a Chrome extension to sync to the server, that way
they'll be searchable from within the app (they're actually extremely easy to make).
* Objectives
** Humane
Software should be ethical, as per the CHT (https://www.humanetech.com). It
should place high value on the user's rights, resources, and freedoms.
Respecting their limited time and energy. Getting out of the way and becoming as
invisible as possible.
** Delightful
Software should spark joy in its users. It should be fun to use as well as
useful. It should be beautiful and stylish.
** Welcoming
It should be a warm, cosy place for the user to store their significant
information. The user should feel at home within the app. It should fit in
to their lives comfortably and familially.
** Understandable
Users should be able to understand - to build a mental model of and reason
about - the software. It should get out of their way and provide seamless
abstractions that empower them to do their work (formal or informal). To be
understandable necessitates being simple - that is, disentangled. Built out of
small, elemental parts that fit together coherently.
** Long-lasting
It should be built upon, and build, sustainable foundations. Users should be
able to keep running it, keep using it, for many years. It should last as Long
as they do.
** Information-oriented
Information (data) is the universal language of computation. It should be based
on a clear and precise informational semantic model.
** Relational
Information means nothing on its own. It should make the different contexts
around and connections between information clear and accessible. I need to think
especially carefully about how the information stored is combined, related
together, and composed. How it can be very useful to see the same information
from multiple independent points of view.
** Interactive and expressive
Make use of ideas from the fields of cognitive science and interaction design.
Let intuition and imagination run free and fast. Do not impose arbitrary limits
on the user's cognitive and behavioural agency. Make the UI as tangible and
manipulable as feasible. Bridge the gulfs of evaluation and execution.

It should leave room for the user's self-expression, giving them maximum
control over positioning, hierarchies, and compositions of notes. It should
let them arrange things to suit their particular way of thinking, just like a
living room. If helpful, it should permit notes of notes, perhaps groups of
notes being notes themselves.
** Networked
"Networked thinking is an explorative approach to problem-solving, whose aim is
to consider the complex interactions between nodes and connections in a given
problem space. Instead of considering a particular problem in isolation to
discover a pre-existing solution, networked thinking encourages non-linear,
second-order reflection in order to let a new idea emerge.

Thinking in networks can be done at an individual level, but the power of
networked thinking becomes apparent in a collaborative setting, where each
individual contributes to the creation of new branches and the addition of new
connections between existing nodes."

[[https://nesslabs.com/networked-thinking]]
* Non-Objectives
** Domineering
I will not follow the example of Facebook and TikTok. The software needs to
respect the user's time and energy, not profit from extracting as much of it
as possible. The software needs to respect the user's views, not manipulate
them.
** Dull
This is a very interesting problem, and the solution ought to be that much at
least. If it's boring to use, then I will have failed at my job. Same with if
it's ugly.
** Complex
If the user cannot understand this software, they will never be able to use it
to its fullest potential.
** Transient
I have little interest in building something that will only be used for a week
and then given up on. It needs to reward the user for the time spent using it.
It needs to earn the user's trust and loyalty. Win them heart and soul.
** Misinforming
Give me truth at any cost. Give the user truth at any cost.
* Food for thought
** Flow
- Create datums from input? (at the time or later) e.g.
  - "Justin Welby - Reimagining Britain" ->
    [:author "Justin Welby"] [:title "Reimagining Britain"]
  - "concert tomorrow" ->
    [:title "concert"] [:time "tomorrow"]
  - How should these new datums be related back to the original input?
    links? children?
- Connect datums to entities? i.e signal that certain datums are
  all about the same thing
** Compositionality
Think about and design the connecting points, the relations between and
compositions of notes. Pay attention to shared structure, universal
interactions.
** Visualisation
How many ways can the user see, compare, arrange, and navigate between notes?
Can the same note be viewed in several different ways? (yes, ideally)
** What are notes?
A 'note' is really a collection of resources that the user is trying to keep
track of. A resource can be text and links, images, videos, files, and other
notes. Resources can be seen individually or in the context of a note.
Resources are first-class: they have their own independent existence and can
be referenced from multiple notes. Zoning in to a resource shows all the notes
that reference it.

The model of notes having fields, which start out empty or with a
default value, that are then edited in context doesn't really make sense, since
multiple notes can share the same fields, and the same values for those
fields. A change of a field in one particular note may actually be valid
across a wide set of other notes. Also, showing empty fields is needless:
you either have a fact to include, or you don't. The 'empty' fields simply
shouldn't exist.

See also [[file:spec/note.org]], [[file:spec/resource.org]], and [[file:spec/fact.org]].
** (Un)structure of notes
The database should fundamentally be an unstructured, unordered list. A specific
structure can then be imposed on this list from the outside, without forcing
that structure on the list itself. A different structure could be added
separately to that. The user should be able to make decisions about structure,
relationship as context-locally as possible, both space- and time-wise. This
means they should be able to structure datums as late as possible (it should not
be fixed ahead of time), and they should be able to add structure to whatever
they are currently thinking about, at the time they are thinking about it, and
that should not prevent different structures from being created at a later time.
** Metametadatadata
Broadly speaking, there are two kinds of data used in representing personal
information. There is the data - that data which is substantive, which forms the
basis for any notion of content (usable or noteworthy information). The data
answers the questions of what (are we thinking about here) and how (are we
thinking about it). Then there is the metadata, the data which represents
everything else about that data, conveying its context and purpose or identity
within that context. It answers the complementary questions of why (are we
thinking about this) and where (are we thinking about it), which are essential
for understanding the data, yet not essential to it itself. The metadata are
context-specific interpretation hints.
** Zettelkasten
- [[https://writingcooperative.com/zettelkasten-how-one-german-scholar-was-so-freakishly-productive-997e4e0ca125?gi=44418e1d73f7]]
- [[https://zettelkasten.de/introduction/]]
- [[https://blog.jethro.dev/posts/zettelkasten_with_org/]]
- [[https://blog.jethro.dev/posts/introducing_org_roam/]]
- [[https://www.reddit.com/r/RoamResearch/comments/eho7de/building_a_second_brain_in_roamand_why_you_might/]]
- [[https://www.nateliason.com/blog/roam]]
- [[http://www.orgzly.com/help]]
- [[https://orgmode.org/manual/Publishing.html]]
- [[https://github.com/jkitchin/org-ref]]
- [[https://orgmode.org/worg/org-contrib/babel/intro.html]]
- [[https://github.com/rexim/org-cliplink]]
- [[https://github.com/abo-abo/org-download]]
- [[https://orgmode.org/manual/Capture.html]]
- [[https://docs.gitlab.com/ee/user/project/wiki/]]
- [[https://mstempl.netlify.app/post/static-website-with-emacs-and-hugo/]]
- [[https://www.theatlantic.com/magazine/archive/1945/07/as-we-may-think/303881/]]
** Motivating examples
- Book: Author(s), Title(s), ISBN, Price(s), Publisher(s)
- Quotation: Author, Content, Source (could be multiple versions of source, like
  Bible translations)
- Article: Author(s), URL (divided into host and path, so that articles can be
  grouped by host), Content, Metadata
- Words, definitions, excerpts: Term/Topic, Description, Source, Metadata,
  related content such as images
** Aggregates / Optionality
- [[https://www.youtube.com/watch?v=YR5WdGrpoug]]
- Separate the individual pieces of information from the schema of notes (the
  particular aggregations) from the contextual, particular subsets required
- Selection probably shouldn't need to know about the shape of the entire tree,
  just that some lower level has the requisite information (::zip rather than
  {::addr [::zip]}.
* Reference material
- [[https://www.w3.org/TR/rdf11-primer/]]
- [[https://schema.org/]]
- [[http://xmlns.com/foaf/spec/]]
- [[https://www.w3.org/2009/08/skos-reference/skos.html]]
- [[https://www.dublincore.org/specifications/dublin-core/dcmi-terms/]]
- [[https://www.w3.org/2000/01/rdf-schema]]
- [[https://www.w3.org/TR/2013/REC-sparql11-overview-20130321/]]
- [[http://sioc-project.org]]
- [[https://www.wikidata.org/wiki/Wikidata:Main_Page]]
- [[https://wiki.dbpedia.org/about]]
- [[https://www.europeana.eu/en/galleries/tranquil-spaces]]
- [[http://www.imagesnippets.com/lio/lio.owl#]]
- [[https://en.m.wikipedia.org/wiki/Pragmatics]]
- [[https://solidproject.org]]
- [[https://noeldemartin.github.io/media-kraken/login]]
- [[https://datatracker.ietf.org/doc/html/rfc1630]]
- [[https://datatracker.ietf.org/doc/html/rfc1737]]
- [[https://datatracker.ietf.org/doc/html/rfc2141]]
- [[https://datatracker.ietf.org/doc/html/rfc3986]]
- [[https://datatracker.ietf.org/doc/html/rfc3987]]
* Prior Art
- [[https://todoist.com]]
- [[https://habitica.com]]
- [[https://orgmode.org]]
- [[https://www.datomic.com]]
- [[https://git-scm.com]]
- [[https://gitlab.com/Numergent/memento]]
- [[https://vivaldi.com]]
- [[https://www.notion.so]]
- [[https://roamresearch.com]]
- [[https://notepod.vincenttunru.com]]
- [[https://gitlab.com/vincenttunru/notepod/-/tree/master]]
- [[https://nette.io/]]
- [[https://www.remnote.com/]]
