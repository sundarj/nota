- Use Solid
- Use RDF dataset for ui
* logo
- something like arrows pointing to a central block, for 'common' place, or
like Material Icons' 'category' icon
* python commonplace
- implement 'juxtapose' operation - anonymous resource collections
- implement 'merge' operation - combine multiple resources into one
- implement search
- implement tags
- implement a log of operations
- implement a note type? a way of gathering resources and commenting on them
- implement links with titles between resources? like Roam AZ-900 page
- consider adding an optional titie field to resources
