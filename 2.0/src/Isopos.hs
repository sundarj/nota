module Isopos ( prerender
              , Renderable (..)
              , DB
              , title
              , description
              , seeAlso
              , selfref
              , shortPredicate
              , nodeText
              , text
              ) where

import Veldi.Isopos
import Veldi.Isopos.Data
