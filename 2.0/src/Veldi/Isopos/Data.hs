module Veldi.Isopos.Data ( DB
                         , title
                         , description
                         , seeAlso
                         , selfref
                         , shortPredicate
                         , nodeText
                         , text
                         , Renderable (..)
                         ) where

import Data.RDF (RDF)
import qualified Data.RDF as RDF
import qualified Data.RDF.Types as RDF
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Map (Map)
import qualified Data.Map as Map

type DB = RDF RDF.AdjHashMap

title :: RDF.Node
title =
  RDF.unode $ Text.pack "http://purl.org/dc/terms/title"

description :: RDF.Node
description =
  RDF.unode $ Text.pack $
    "http://purl.org/dc/terms/description"

seeAlso :: RDF.Node
seeAlso =
  RDF.unode $ Text.pack $
      "http://www.w3.org/2000/01/rdf-schema#seeAlso"

selfref :: String -> RDF.Node
selfref ident = 
 RDF.unode $ Text.pack $ "https://veldi.dev/" <> ident

predicateAliases :: Map Text Text
predicateAliases =
  Map.fromList [ ( Text.pack "http://purl.org/dc/terms/title"
                 , Text.pack "title"
                 )
               , ( Text.pack "http://purl.org/dc/terms/description"
                 , Text.pack "description"
                 )
               , ( Text.pack "http://purl.org/dc/terms/source"
                 , Text.pack "source"
                 )
               , ( Text.pack "http://www.w3.org/2000/01/rdf-schema#seeAlso"
                 , Text.pack "linked"
                 )
               ]

shortPredicate :: Text -> Text
shortPredicate predicate =
  case Map.lookup predicate predicateAliases of
    Nothing -> predicate
    Just p -> p

nodeText :: RDF.Node -> Maybe Text
nodeText (RDF.UNode txt) = Just txt
nodeText (RDF.BNode txt) = Just txt
nodeText (RDF.BNodeGen _) = Nothing
nodeText (RDF.LNode lit) =
  case lit of
    RDF.PlainL txt -> Just txt
    RDF.PlainLL txt _lng -> Just txt
    _ -> Nothing

{-
RDF.triple (RDF.unode $ Text.pack "one") (RDF.unode $ Text.pack "title") (RDF.LNode $ RDF.PlainL $ Text.pack "reimagining britain")
Triple (UNode "one") (UNode "title") (LNode (PlainL "reimagining britain"))
-}
text :: RDF.Node -> RDF.Node -> String -> RDF.Triple
text ident prd obj =
  RDF.triple
    ident
    prd
    (RDF.LNode $ RDF.PlainL $ Text.pack obj)

data Renderable = MkScalar String
                | MkComposite [Renderable]
                deriving stock (Show)

