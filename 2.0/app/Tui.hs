module Tui where

import qualified Brick.AttrMap as Attrs
import qualified Brick.Main as Br
import qualified Brick.Types as BrTypes
import qualified Brick.Widgets.Core as Widgets
import qualified Brick.Widgets.Center as Widgets
import qualified Brick.Util as BrUtil
import qualified Graphics.Vty.Input.Events as Events
import qualified Graphics.Vty.Attributes as Attrs
import qualified Graphics.Vty.Attributes.Color as Color
import qualified Data.List as List
import qualified Data.RDF as RDF
import qualified Data.RDF.Query as RDFQ
import qualified Data.Text as Text
import qualified Text.Read as Read
import Control.Monad (forM_)
import Data.Function ((&))
import Control.Arrow ((<<<), (>>>))
import Debug.Trace (traceShowId)
import qualified Cursor.List as Cursor
import Control.Monad.State.Class (MonadState (get, put))

import Isopos ( DB
              , title
              , description
              , seeAlso
              , selfref
              , shortPredicate
              , nodeText
              , text
              )
  
data ResourceName =
  MkResourceName
  deriving stock (Show, Eq, Ord)

data View = MkIndexView | MkResourceView RDF.Triples
          deriving stock (Show)

data AppState =
  MkAppState
    { appDB :: DB
    , appCursor :: Cursor.ListCursor RDF.Triples
    , appCurrentView :: View
    }
  deriving stock (Show)

buildInitialState :: IO AppState
buildInitialState = 
  pure $ MkAppState
    { appDB = db
    , appCursor =
        Cursor.makeListCursor $ resources db
    , appCurrentView = MkIndexView
    }
      where
        resources =
          (RDF.triplesOf
            >>> (List.groupBy RDFQ.equalSubjects))

db :: DB
db = RDF.empty
     & (addTriple $
         text
         (selfref "one")
         title
         "Reimagining Britain")
    & (addTriple $
         text
         (selfref "one")
         description
         "by Justin Welby")
    & (addTriple $
        text
        (selfref "two")
        description
        "Simple Made Easy")
    & (addTriple $
        text
        (selfref "three")
        description
        "Hammock Driven Development")
    & (addTriple $ RDF.triple
        (selfref "four")
        seeAlso
        (selfref "one"))
    & (addTriple $ RDF.triple
        (selfref "four")
        seeAlso
        (selfref "two"))
    & (addTriple $
        text
        (selfref "five")
        description
        "Alice was beginning to get very tired of sitting by her sister on the bank, and of having nothing to do. Once or twice she had peeped into the book her sister was reading, but it had no pictures or conversations in it, \"and what is the use of a book,\" thought Alice, \"without pictures or conversations?\"\n\n\
\So she was considering in her own mind (as well as she could, for the day made her feel very sleepy and stupid), whether the pleasure of making a daisy-chain would be worth the trouble of getting up and picking the daisies, when suddenly a White Rabbit with pink eyes ran close by her.\n\n\
\There was nothing so very remarkable in that, nor did Alice think it so very much out of the way to hear the Rabbit say to itself, \"Oh dear! Oh dear! I shall be too late!\" But when the Rabbit actually took a watch out of its waistcoat-pocket and looked at it and then hurried on, Alice started to her feet, for it flashed across her mind that she had never before seen a rabbit with either a waistcoat-pocket, or a watch to take out of it, and, burning with curiosity, she ran across the field after it and was just in time to see it pop down a large rabbit-hole, under the hedge. In another moment, down went Alice after it!\n\n\
\The rabbit-hole went straight on like a tunnel for some way and then dipped suddenly down, so suddenly that Alice had not a moment to think about stopping herself before she found herself falling down what seemed to be a very deep well.\n\n\
\Either the well was very deep, or she fell very slowly, for she had plenty of time, as she went down, to look about her. First, she tried to make out what she was coming to, but it was too dark to see anything; then she looked at the sides of the well and noticed that they were filled with cupboards and book-shelves; here and there she saw maps and [Pg 5]pictures hung upon pegs. She took down a jar from one of the shelves as she passed. It was labeled \"ORANGE MARMALADE,\" but, to her great disappointment, it was empty; she did not like to drop the jar, so managed to put it into one of the cupboards as she fell past it.")
  where addTriple = flip RDF.addTriple

preview :: RDF.Triples -> String
preview resource = 
  "resource of " <> (show $ length resource) <> " facts"

drawIndexView
  :: Cursor.ListCursor RDF.Triples
  -> [BrTypes.Widget ResourceName]
drawIndexView cur =
  let drawResource resource =
        let text = Widgets.withAttr
                    (Attrs.attrName "resource") $
                      Widgets.padAll 1 $
                        Widgets.str $ preview resource
        in case cur of
          (Cursor.ListCursor
            { Cursor.listCursorPrev
            , Cursor.listCursorNext
            }) -> 
              case listCursorPrev of
                [] -> text 
                (r:_) ->
                  if resource == r then
                    Widgets.forceAttr
                      (Attrs.attrName "selected") $
                        text
                  else
                    text
  in [Widgets.hCenter $
       Widgets.vBox $ Cursor.rebuildListCursor $
         fmap drawResource cur]

drawResourceView
  :: RDF.Triples -> [BrTypes.Widget ResourceName]
drawResourceView resource =
  let main' =
        Widgets.withVScrollBars BrTypes.OnRight
        <<< Widgets.viewport MkResourceName BrTypes.Vertical
        <<< Widgets.visible
        <<< Widgets.hCenter
        <<< Widgets.withAttr (Attrs.attrName "main")
        <<< Widgets.padAll 1
        <<< Widgets.vBox
      indexed = zip [1..] resource
  in [main' $
        flip map indexed $ \case
          (idx, RDF.Triple sbj prd obj) ->
            case nodeText prd of
              Nothing -> Widgets.emptyWidget
              Just prd' ->
                case shortPredicate prd' of
                  Nothing -> Widgets.txt prd'
                  Just prd'' ->
                    case nodeText obj of
                      Nothing -> Widgets.emptyWidget
                      Just obj' ->
                        let prdPad = Widgets.Pad
                                       (if idx > 1 then
                                          2
                                        else
                                          0)
                        in Widgets.vBox
                            [ Widgets.padTop
                               prdPad $
                                 Widgets.withAttr
                                   (Attrs.attrName "predicate") $
                                     Widgets.txt prd''
                            , Widgets.padTop
                                (Widgets.Pad 1) $
                                  Widgets.txtWrap obj'
                            ]
     ]

drawApp :: AppState -> [BrTypes.Widget ResourceName]
drawApp st =
  case appCurrentView st of
    MkIndexView -> drawIndexView $ appCursor st 
    MkResourceView r -> drawResourceView r

handleEvent :: BrTypes.BrickEvent n e
            -> BrTypes.EventM n AppState ()
handleEvent e = do
  case e of
    BrTypes.VtyEvent vtye ->
      case vtye of
        Events.EvKey Events.KEsc [] -> Br.halt
        _ -> pure ()
    _ -> pure ()
  st <- get
  case appCurrentView st of
    MkIndexView ->
      case e of
        BrTypes.VtyEvent vtye ->
          case vtye of
            Events.EvKey Events.KUp [] -> do
              let cur = appCursor $ st
              case Cursor.listCursorSelectPrev cur of
                Nothing -> pure ()
                Just cur' ->
                  put (st { appCursor = cur' })
            Events.EvKey Events.KDown [] -> do
              let cursor = appCursor $ st
              case Cursor.listCursorSelectNext cursor of
                Nothing -> pure ()
                Just cursor' ->
                  put (st { appCursor = cursor' })
            Events.EvKey Events.KEnter [] -> do
              let cursor = appCursor $ st
              case cursor of
                (Cursor.ListCursor
                  { Cursor.listCursorPrev
                  , Cursor.listCursorNext
                  }) ->
                  case listCursorPrev of
                    [] -> pure ()
                    (r:_) ->
                      put (st
                        { appCurrentView = MkResourceView r })
            _ -> pure ()
        _ -> pure ()
    _ -> pure ()
app :: Br.App AppState e ResourceName
app =
  Br.App
    { Br.appDraw = drawApp
    , Br.appChooseCursor = Br.showFirstCursor
    , Br.appHandleEvent = handleEvent
    , Br.appStartEvent = pure ()
    , Br.appAttrMap = const $
        Attrs.attrMap
          Attrs.defAttr
          [ ( Attrs.attrName "main"
            , BrUtil.bg $ Color.srgbColor 241 241 241
            )
          , ( Attrs.attrName "resource"
            , BrUtil.bg $ Color.srgbColor 241 241 241
            )
          , ( Attrs.attrName "selected"
            , BrUtil.bg $ Color.srgbColor 236 236 236
            )
          , ( Attrs.attrName "predicate"
            , BrUtil.style Attrs.bold
            )
          , ( Attrs.attrName "predicate"
            , Attrs.withStyle
                (BrUtil.bg $ Color.srgbColor 241 241 241)
                Attrs.bold
            )
          ]
    }

main :: IO ()
main = do
  initialState <- buildInitialState
  endState <- Br.defaultMain app initialState
  print endState


