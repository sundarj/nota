{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Web (main) where

import qualified Network.Wai as Wai
import qualified Network.Wai.Handler.Warp as Warp
import qualified Servant as Serv
import Servant ((:>) (..), (:<|>) (..))
import qualified WaiAppStatic.Types as WAST
import WaiAppStatic.Storage.Filesystem (defaultWebAppSettings)
import qualified Servant.HTML.Lucid as Serv
import Lucid
import Lucid.Base
import Control.Arrow ((<<<), (>>>))
import Data.Function ((&))
import Data.Map (Map)
import qualified Data.Map as Map
import qualified Data.List as List
import qualified Data.Text as Text
import Data.Proxy (Proxy (..))
import Control.Monad (forM, forM_)
import qualified Data.RDF as RDF
import qualified Commonmark as Commonmark
import qualified Commonmark.Extensions.Footnote as Commonmark
import qualified Web.FormUrlEncoded as FUE
import GHC.Generics (Generic)
import Control.Monad.IO.Class (liftIO)
import Data.IORef ( IORef
                  , newIORef
                  , readIORef
                  , modifyIORef
                  )
import qualified Control.Monad.Error.Class as ME
import Data.ByteString (ByteString)
import qualified Data.Aeson as Aeson
import qualified Data.Text.Lazy.Encoding as Enc
import qualified Data.Text.Lazy as Text.Lazy

import Isopos ( DB
              , title
              , description
              , seeAlso
              , selfref
              , shortPredicate
              , nodeText
              , text
              )

layout :: Html () -> Html ()
layout child = do
  doctype_
  html_ [lang_ "en"] $ do
    head_ $ do
       meta_ [charset_ "utf-8"]
       meta_ [ name_ "viewport"
             , content_ "width=device-width, initial-scale=1.0"
             ]
       title_ "Isopos"
       link_ [ rel_ "preconnect"
             , href_ "https://fonts.gstatic.com"
             ]
       link_ [ rel_ "stylesheet"
             , href_ "https://fonts.googleapis.com/css2?family=IBM+Plex+Sans:wght@400;500&display=swap"
             ]
       link_ [ rel_ "stylesheet"
             , href_ "https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200&icon_names=check,edit"
             ]
       link_ [ rel_ "stylesheet"
             , href_ "/assets/styles/index.css?42"
             ]
       term "script" [src_ "https://unpkg.com/@tailwindcss/browser@4"] mempty
    body_ $ do
      nav_ [class_ "text-end px-4 py-2"] $ do
        a_ [href_ "/", class_ "inline-block mr-4"] "Index"
        a_ [ href_ "/new"
           , class_ "inline-block"
           ]
           "New"
      main_ [class_ "p-4"] $ do
        child
      term "script" [src_ "https://unpkg.com/marked@15.0.7/lib/marked.umd.js"] mempty
      term "script" [src_ "/assets/scripts/index.js"] mempty

preview :: [RDF.Triple] -> String
preview resource =
  let
    len = length resource
    hasTitle = RDF.predicateOf >>> (== title)
  in
    if len < 100 then
      case filter hasTitle resource of
        [] -> "resource of " <> (show len) <> " facts"
        ((RDF.Triple _ _ obj):_) ->
          case nodeText obj of
            Nothing ->
              "resource of " <> (show len) <> " facts"
            Just title' ->
              (Text.unpack title')
                <> " (" <> (show len) <> " facts)"
    else
      "resource of " <> (show len) <> " facts"


indexView :: DB -> Html ()
indexView db = layout $ do
  header_ [role_ "banner", class_ "text-center"] $ do
    h1_ $ do
      img_ [ src_ "/assets/images/category.svg"
           , alt_ "a digital commonplace book"
           , class_ "inline-block mr-2"
           ]
      span_ [class_ "font-bold"] "Isopos"
  let trips = RDF.triplesOf db
  let resources = List.groupBy RDF.equalSubjects trips
  ul_ [class_ "text-center"] $ do
    forM_ resources $ \resource ->
      case resource of
        [] -> li_ "empty resource"
        ((RDF.Triple sbj _ _):_) ->
          let
            stxt = case nodeText sbj of
                    Nothing -> ""
                    Just t -> t
          in
            li_ $
              a_ [ href_ $ "/resource/" <> stxt
                 , class_ "no-underline block bg-gray-50 p-2 my-4 border border-gray-300"
                 ] $
                   toHtml $ preview resource

getNewView :: DB -> Bool -> Html ()
getNewView db success =
  let trips = RDF.triplesOf db
  in layout $ do
       if success then
        div_ [class_ "bg-green-300 p-4 mb-2"]
          "Created new resource"
       else
         pure ()
       form_ [ action_ "/new"
             , method_ "post"
             ] $ do
               div_ $ do
                 label_ [for_ "subject"] "Resource"
                 input_ [ type_ "text"
                        , name_ "ftSubject"
                        , id_ "subject"
                        , class_ "block w-full bg-gray-50 p-1 border border-gray-300"
                        , list_ "subjects"
                        , required_ "required"
                        ]
                 datalist_ [id_ "subjects"] $ do
                   let subs = map (\(RDF.Triple sbj _ _)   -> sbj) trips
                   forM_ (List.nub subs) $ \sbj ->
                     option_ $
                       case nodeText sbj of
                         Nothing -> "<nil>"
                         Just sbj' -> toHtml sbj'
               div_ [class_ "mt-4"] $ do
                 label_ [for_ "predicate"] "Property"
                 input_ [ type_ "text"
                        , name_ "ftPredicate"
                        , id_ "predicate"
                        , class_ "block w-full bg-gray-50 p-1 border border-gray-300"
                        , list_ "predicates"
                        , required_ "required"
                        ]
                 datalist_ [id_ "predicates"] $ do
                   let preds = map (\(RDF.Triple _ prd _) -> prd) trips
                   forM_ (List.nub preds) $ \prd ->
                     option_ $
                       case nodeText prd of
                         Nothing -> "<nil>"
                         Just prd' -> toHtml prd'
               div_ [class_ "mt-4"] $ do
                 label_ [for_ "object"] "Value"
                 textarea_ [ name_ "ftObject"
                           , id_ "object"
                           , class_ "block w-full bg-gray-50 p-1 border border-gray-300"
                           , rows_ "10"
                           , required_ "required"
                           ]
                           ""
               div_ [class_ "mt-4"] $
                 button_ [ type_ "submit"
                         , class_ "bg-sky-200 p-2"
                         ] "Save"

data FormTriple =
  MkFormTriple
    { ftSubject :: String
    , ftPredicate :: String
    , ftObject :: String
    }
  deriving (Eq, Show, Generic)

instance FUE.FromForm FormTriple

resourceView :: DB -> [String] -> Html ()
resourceView db parts = layout $ do
  let sbj = foldl' (\acc s -> acc ++ s ++ "/") "" parts
  let sbj' = init sbj
  let trips = RDF.triplesOf db
  let resources = List.groupBy RDF.equalSubjects trips
  let resource = filter
                   (\case
                     [] -> False
                     ((RDF.Triple s _ _):_) ->
                       case nodeText s of
                         Nothing -> False
                         Just s' -> s' == Text.pack sbj')
                   resources
  article_ [class_ "c-resource"] $
    case resource of
      [] -> h1_ "no resource found"
      (r:_) -> do
        h1_ $ toHtml sbj'
        dl_ $
          forM_ r $
            \(RDF.Triple _ prd obj) -> do
              let prd'' = case nodeText prd of
                            Nothing -> "<nil>"
                            Just prd' -> prd'
              let obj'' = case nodeText obj of
                            Nothing -> "<nil>"
                            Just obj' -> obj'
              let trip = [ Text.pack sbj'
                         , prd''
                         , obj''
                         ]
              div_ [data_ "triple" $
                     Text.Lazy.toStrict $
                       Enc.decodeUtf8 $
                         Aeson.encode trip] $ do
                dt_ [class_ "c-resource__predicate-node font-bold mt-4"] $ do
                  span_ [class_ "js-predicate-text"] $
                    toHtml $ shortPredicate prd''
                  button_ [ type_ "button"
                          , class_ "c-resource__edit-btn"
                          ] $
                    span_ [class_ "material-symbols-outlined"] "edit"
                dd_ [class_ "c-resource__object-node"] $
                  if prd == seeAlso then
                    a_ [ href_ $ "/resource/" <> obj''
                       , class_ "text-cyan-600 hover:underline"
                       ] $
                         toHtml obj''
                  else
                    let tokens = Commonmark.tokenize
                                   "triple"
                                   obj''
                        result = Commonmark.parseCommonmarkWith
                                   (Commonmark.defaultSyntaxSpec
                                     <> Commonmark.footnoteSpec)
                                   tokens
                    in do
                      res <- result
                      case res of
                        Left err -> toHtml $ show err
                        Right (html :: Commonmark.Html ()) ->
                          toHtmlRaw $
                            Commonmark.renderHtml html

data EditRequest =
  MkEditRequest
    { erOldPredicate :: String
    , erOldObject :: String
    , erSubject :: String
    , erPredicate :: String
    , erObject :: String
    }
  deriving (Eq, Show, Generic)

instance Aeson.FromJSON EditRequest

type AppRoutes = Serv.Get '[Serv.HTML] (Html ())
                 :<|> "resource"
                   :> Serv.CaptureAll "subject" String
                   :> Serv.Get '[Serv.HTML] (Html ())
                 :<|> "new"
                   :> Serv.QueryFlag "success"
                   :> Serv.Get '[Serv.HTML] (Html ())
                 :<|> "new"
                   :> Serv.ReqBody '[Serv.FormUrlEncoded] FormTriple
                   :> Serv.Post '[Serv.HTML] (Html ())
                 :<|> "edit"
                   :> Serv.ReqBody '[Serv.JSON] EditRequest
                   :> Serv.Put '[Serv.JSON] ()

type FileRoutes = "assets" :> Serv.Raw
type Routes = AppRoutes :<|> FileRoutes

redirect
  :: ME.MonadError Serv.ServerError m
  => ByteString
  -> m a
redirect path =
  ME.throwError $
    Serv.err301
      { Serv.errHeaders = [( "Location", path )] }

appServer :: IORef DB -> Serv.Server AppRoutes 
appServer dbRef =
  indexHdr
  :<|> resourceHdr
  :<|> getNewHdr
  :<|> postNewHdr
  :<|> editHdr
  where
    indexHdr = do
      db <- liftIO $ readIORef dbRef           
      pure $ indexView db
    resourceHdr sbj = do
      db <- liftIO $ readIORef dbRef
      pure $ resourceView db sbj
    getNewHdr success = do
      db <- liftIO $ readIORef dbRef
      pure $ getNewView db success
    postNewHdr formTriple = do
      let sbj = RDF.unode $ Text.pack $
                  ftSubject formTriple
      let prd = RDF.unode $ Text.pack $
                  ftPredicate formTriple
      let obj = RDF.LNode $ RDF.PlainL $ Text.pack $
                  ftObject formTriple
      let trip = RDF.triple sbj prd obj
      liftIO $ modifyIORef dbRef (flip RDF.addTriple trip)
      redirect "/new?success"
    editHdr editRequest = do 
      let sbj = RDF.unode $ Text.pack $
                  erSubject editRequest
      let prd = RDF.unode $ Text.pack $
                  erOldPredicate editRequest
      let obj = RDF.LNode $ RDF.PlainL $ Text.pack $
                  erOldObject editRequest
      let trip = RDF.triple sbj prd obj
      let prd' = RDF.unode $ Text.pack $
                  erPredicate editRequest
      let obj' = RDF.LNode $ RDF.PlainL $ Text.pack $
                  erObject editRequest
      let trip' = RDF.triple sbj prd' obj'
      liftIO $ modifyIORef dbRef $ (flip RDF.removeTriple trip) >>> (flip RDF.addTriple trip')
      pure ()



-- serveDirectoryWebApp caches aggressively
fileServer :: Serv.Server FileRoutes
fileServer = Serv.serveDirectoryWith settings
  where settings = (defaultWebAppSettings "assets")
                     { WAST.ssMaxAge = WAST.NoCache }

app :: IORef DB -> Wai.Application
app dbRef = Serv.serve
              (Proxy :: Proxy Routes)
              (appServer dbRef :<|> fileServer)

main :: IO ()
main = do
  let p = RDF.TurtleParser
            (Just $ RDF.BaseUrl "https://veldi.dev")
            Nothing
  res <- RDF.parseFile p "assets/isopos.ttl"
  case res of
    Left (RDF.ParseFailure msg) ->
      putStrLn $ "error loading db: " <> msg
    Right db -> do
      ref <- newIORef db
      Warp.run 3000 (app ref)

